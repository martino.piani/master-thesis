#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Model running file

Created on Mon Mar 14 15:43:42 2022

@author: Martino Piani, martino.piani@rwth-aachen.de
"""

from WEF_Opt import wef_opt

scenarios = {"BSH":"NPC", # dictionaries with scenario and relative objective
             "BSL":"NPC",
             "WS":"TWU",
             "CSE":"GWP",
             "CS":"GWP"}

for scenario in list(scenarios.keys()):
   result = wef_opt(scenarios[scenario], scenario)
