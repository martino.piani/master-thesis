#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 23:23:41 2022

@author: Martino Piani, martino.piani@rwth-aachen.de
"""

from constraints import tot_investment,tot_operation

#%% Environment

def total_water_use(model): 
    
    twe = sum(sum(model.water_e[t,e] for e in model.E) for t in model.T)
    twbd = sum(sum(model.water_bd[t,b] for b in model.BD) for t in model.T)
    twbe = sum(sum(model.water_be[t,b] for b in model.BE) for t in model.T)
    
    return twe+twbd+twbe

def total_co2_emissions(model): 
    
    t_co2_el = sum(sum(model.co2_e[t,e] for e in model.E) for t in model.T)
    t_co2_bd = sum(sum(model.co2_bd[t,b] for b in model.BD) for t in model.T)
    t_co2_be = sum(sum(model.co2_be[t,b] for b in model.BE) for t in model.T)
    
    return t_co2_el + t_co2_bd + t_co2_be

#%% Economy

def net_present_cost(model):
    """
    Define a function that calculates the net present cost of the model.
    Inflation i is not considered in the annual total cost ATC. 
    Adjusted annual total cost: ATTC = ATC * (1+i)**n.
    """
    NPC = sum((tot_investment(model,t) + tot_operation(model,t))/ (1+model.r)**(t-min(model.T)) for t in model.T)
    
    return NPC