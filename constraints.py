#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 31 23:04:45 2022

@author: Martino Piani, martino.piani@rwth-aachen.de

Constraints for the WEF Nexus model for Ethiopia
"""

def initial_capacity(model, t, e):
    """ 
    Requires that the already installed capacity is maintained. 
    Decommissioning of exixting plants is not featured. 
    """
    return model.c_e[t,e] >= model.C_E_INIT[e]
    
def elec_production(model, t, e):
    
    """ Returns the total electricity produced in a given year with a given technology. """
    
    return model.L[t,e] == 8760*model.CF[e]*model.c_e[t, e] #MWh

def elec_demand(model, t):
    
    """ Requires that the total electricity produced is enough to meet the demand and the export target. """
    
    return sum(model.L[t,e] for e in model.E) >= model.ED[t]

"""
Biodiesel and bioethanol are separated because they have two different demand time series.
This way it is possible to have multiple technologies for each.
"""

def BD_demand(model, t):

    return sum(model.c_bd[t,b] for b in model.BD) >= model.BDD[t]

def BE_demand(model, t):

    return sum(model.c_be[t,b] for b in model.BE) >= model.BED[t]

"""Auxiliary variable constraints:"""

def water_for_electricity(model,t,e):
    return model.water_e[t,e] == model.w_E[e] * model.L[t,e]

def water_for_biodiesel(model,t,b): #Var(model.T, model.BD, within=NonNegativeReals)
    return model.water_bd[t,b] == (model.w_BD[b] + model.w_f_BD[b]) * model.c_bd[t,b]

def water_for_bioethanol(model,t,b):
    return model.water_be[t,b] == (model.w_BE[b] + model.w_f_BE[b]) * model.c_be[t,b]

#%% Investment Costs

def investment_elec(model,t,e):
    if t == min(model.T):
        return model.inv_cost_e[t,e] == (model.c_e[t,e] - model.C_E_INIT[e])*model.CAPEX_E[e]
    else:
        return model.inv_cost_e[t,e] == (model.c_e[t,e] - model.c_e[t-1,e]) * model.CAPEX_E[e]
   
def investment_biodiesel(model,t,b):
    if t == min(model.T):
        return model.inv_cost_bd[t,b] == (model.c_bd[t,b] - model.C_BD_INIT[b])*model.CAPEX_BD[b]
    else: 
        return model.inv_cost_bd[t,b] == (model.c_bd[t,b] - model.c_bd[t-1,b])*model.CAPEX_BD[b]

def investment_bioethanol(model,t,b):
    if t == min(model.T):
        return model.inv_cost_be[t,b] == (model.c_be[t,b] - model.C_BE_INIT[b])*model.CAPEX_BE[b]
    else: 
        return model.inv_cost_be[t,b] == (model.c_be[t,b] - model.c_be[t-1,b])*model.CAPEX_BE[b]


def tot_investment(model,t):
    """
    Calculates the total invesment cost, not the net present cost. 
    Feature to be added.
    """        
    CAPEX_E = sum(model.inv_cost_e[t,e] for e in model.E)
        
    CAPEX_BD = sum(model.inv_cost_bd[t,b] for b in model.BD)
    CAPEX_BE = sum(model.inv_cost_be[t,b] for b in model.BE)
    
    return CAPEX_E + CAPEX_BD + CAPEX_BE


#%% Operation Costs

def operation_elec(model,t,e): 
    return model.op_cost_e[t,e] == model.c_e[t,e]*model.OPEX_E[e]

def operation_biodiesel(model,t,b): 
    return model.op_cost_bd[t,b] == model.c_bd[t,b]*model.OPEX_BD[b]

def operation_bioethanol(model,t,b): 
    return model.op_cost_be[t,b] == model.c_be[t,b]*model.OPEX_BE[b]

def tot_operation(model,t):
    """
    Returns the sum of operation costs for electricity and biofuel production.
    """        
    OPEX_E = sum(model.op_cost_e[t,e] for e in model.E)
        
    OPEX_BD = sum(model.op_cost_bd[t,b] for b in model.BD)
    OPEX_BE = sum(model.op_cost_be[t,b] for b in model.BE) 
    
    return OPEX_E + OPEX_BD + OPEX_BE

#%% CO2 emissions

def co2_elec(model,t,e):
    return model.co2_e[t,e] == model.CO2_E[e] * model.L[t,e]

def co2_biodiesel(model,t,b):
    return model.co2_bd[t,b] == model.CO2_BD[b] * model.c_bd[t,b]

def co2_bioethanol(model,t,b):
    return model.co2_be[t,b] == model.CO2_BE[b] * model.c_be[t,b]

def max_investment(model, t):
    
    """ The investment cost constraint sets an upper limit to the investment costs, given by a share of GDP. It considers only capital costs, as operation costs are generally covered by the revenues. """

    return tot_investment(model, t) <= model.gdp[t]*model.s_max


def generation_potential(model,t,e):
    
    """ Requires that the electricity generation capacity installed does not overtake the estimated potential. """
    
    return model.c_e[t,e] <= model.POT_E[e]

#%% Job creation

def jobc_E(model, t, e, j):
    if j == "FJ":
        return model.jobs_e[t,e,j] == model.L[t,e] * model.EJ[e,j] * 3.6e-6 * model.jobmult[t]
    else:
        return model.jobs_e[t,e,j] == model.c_e[t,e] * model.EJ[e,j] * model.jobmult[t]

def jobc_BD(model, t, bd, j):
    if j == "FJ":
        return model.jobs_bd[t,bd,j] == model.c_bd[t,bd] * model.BJ[j] * 3.6e-6 * model.jobmult[t]
    else:
        return model.jobs_bd[t,bd,j] == model.c_bd[t,bd] * model.BJ[j] * model.jobmult[t]

def jobc_BE(model, t, be, j):
    if j == "FJ":
        return model.jobs_be[t,be,j] == model.c_be[t,be] * model.BJ[j] * 3.6e-6 * model.jobmult[t]
    else:
        return model.jobs_be[t,be,j] == model.c_be[t,be] * model.BJ[j] * model.jobmult[t]