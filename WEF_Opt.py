#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 12:06:51 2021

@author: Martino Piani, martino.piani@rwth-aachen.de

Model declaration file. Here all the model parts are declared and 

"""

#%% Imports

import pandas
import datetime

from initialize import *
from constraints import *
from objectives import *
from postprocessing import postprocessing
 
from pyomo.environ import *
from pyomo.opt import SolverFactory

#%% Function definition

def wef_opt(optimization_type, scenario):

    #%% Model creation
    
    model = AbstractModel()
    
    #%% Demand data preprocessing
    
    model.data = pandas.read_excel("input/"+scenario+"/time_series.xls",index_col="year") 
    
    #%% Model sets
    
    model.T = Set(initialize=init_T) # Set of periods T (years)
    model.E = Set() # Set of electricity sources E
    model.BD = Set() # Set of biodiesel sources BD
    model.BE = Set() # Set of bioethanol sources BE
    model.J = Set(initialize=["MFC", "CI", "OM", "FJ"]) # job categories/types
    
    #%% Model parameters
    
    #Water parameters
    
    model.w_E = Param(model.E, within=NonNegativeReals) # water for electricity
    
    model.w_BD = Param(model.BD, within=NonNegativeReals) # water for biofuel (process)
    model.w_f_BD = Param(model.BD, within=NonNegativeReals) # water for food for biofuel
    
    model.w_BE = Param(model.BE, within=NonNegativeReals) # water for biofuel (process)
    model.w_f_BE = Param(model.BE, within=NonNegativeReals) # water for food for biofuel
        
    #%% Economic Parameters
    
    model.r = Param(within=NonNegativeReals) # discount rate
    
    model.CAPEX_E = Param(model.E, within=NonNegativeReals)
    model.CAPEX_BD = Param(model.BD, within=NonNegativeReals)
    model.CAPEX_BE = Param(model.BE, within=NonNegativeReals)
    
    model.OPEX_E = Param(model.E, within=NonNegativeReals)
    model.OPEX_BD = Param(model.BD, within=NonNegativeReals)
    model.OPEX_BE = Param(model.BE, within=NonNegativeReals)
    
    #%% Demand Parameters
    
    model.ED = Param(model.T, within=NonNegativeReals, initialize=init_ED) 
    model.DED = Param(model.T, within=NonNegativeReals, initialize=init_DED) 
    
    model.BDD = Param(model.T, within=NonNegativeReals, initialize=init_BDD) 
    model.BED = Param(model.T, within=NonNegativeReals, initialize=init_BED) 
    
    #%% Source Parameters
    
    model.CF = Param(model.E, within=NonNegativeReals) # Capacity Factor
    model.C_E_INIT = Param(model.E, within=NonNegativeReals) 
    model.C_BD_INIT = Param(model.BD, within=NonNegativeReals)
    model.C_BE_INIT = Param(model.BE, within=NonNegativeReals)
    model.POT_E = Param(model.E, within=NonNegativeReals) # resource potential
    
    #%% CO2 Parameters
    
    model.CO2_E = Param(model.E, within=NonNegativeReals)
    model.CO2_BD = Param(model.BD, within=NonNegativeReals) 
    model.CO2_BE = Param(model.BE, within=NonNegativeReals) 
    
    #%% Job creation parameters
    
    model.EJ = Param(model.E, model.J, within=NonNegativeReals)
    model.BJ = Param(model.J, within=NonNegativeReals)
    
    model.jobmult = Param(model.T, within=NonNegativeReals, initialize=init_jobmult)
    
    #%% Model Variables
    
    model.c_e  = Var(model.T, model.E,  within=NonNegativeIntegers)
    model.c_bd = Var(model.T, model.BD, within=NonNegativeIntegers)
    model.c_be = Var(model.T, model.BE, within=NonNegativeIntegers)
    
    model.L = Var(model.T, model.E, within=NonNegativeReals)
    
    model.water_e  = Var(model.T, model.E,  within=NonNegativeReals)
    model.water_bd = Var(model.T, model.BD, within=NonNegativeReals)
    model.water_be = Var(model.T, model.BE, within=NonNegativeReals)
    
    model.inv_cost_e  = Var(model.T, model.E,  within=NonNegativeReals)
    model.inv_cost_bd = Var(model.T, model.BD, within=NonNegativeReals)
    model.inv_cost_be = Var(model.T, model.BE, within=NonNegativeReals)
    
    model.op_cost_e  = Var(model.T, model.E,  within=NonNegativeReals)
    model.op_cost_bd = Var(model.T, model.BD, within=NonNegativeReals)
    model.op_cost_be = Var(model.T, model.BE, within=NonNegativeReals)
    
    model.co2_e  = Var(model.T, model.E,  within=NonNegativeReals)
    model.co2_bd = Var(model.T, model.BD, within=NonNegativeReals)
    model.co2_be = Var(model.T, model.BE, within=NonNegativeReals)
    
    # job creation variables
    
    model.jobs_e = Var(model.T, model.E, model.J, within=NonNegativeReals)
    model.jobs_bd = Var(model.T, model.BD, model.J, within=NonNegativeReals)
    model.jobs_be = Var(model.T, model.BE, model.J, within=NonNegativeReals)
    
    #%% Model Constraints 
    
    model.meeting_Edemand  = Constraint(model.T, rule=elec_demand)
    model.meeting_BDdemand = Constraint(model.T, rule=BD_demand)
    model.meeting_BEdemand = Constraint(model.T, rule=BE_demand)
    
    model.initial_capacity   = Constraint(model.T, model.E, rule=initial_capacity)
    model.resource_potential = Constraint(model.T, model.E, rule=generation_potential)
    
    model.elec_production = Constraint(model.T, model.E, rule=elec_production)
    
    model.water_for_electricity = Constraint(model.T, model.E, rule=water_for_electricity)
    model.water_for_biodiesel   = Constraint(model.T, model.BD, rule=water_for_biodiesel)
    model.water_for_bioethanol  = Constraint(model.T, model.BE, rule=water_for_bioethanol)
    
    model.investment_elec       = Constraint(model.T, model.E, rule=investment_elec)
    model.investment_biodiesel  = Constraint(model.T, model.BD, rule=investment_biodiesel)
    model.investment_bioethanol = Constraint(model.T, model.BE, rule=investment_bioethanol)
    
    model.operation_elec       = Constraint(model.T, model.E, rule=operation_elec)
    model.operation_biodiesel  = Constraint(model.T, model.BD, rule=operation_biodiesel)
    model.operation_bioethanol = Constraint(model.T, model.BE, rule=operation_bioethanol)
    
    model.co2_elec       = Constraint(model.T, model.E, rule=co2_elec)
    model.co2_biodiesel  = Constraint(model.T, model.BD, rule=co2_biodiesel)
    model.co2_bioethanol = Constraint(model.T, model.BE, rule=co2_bioethanol)
    
    # constraints for job creation
    
    model.jobc_E = Constraint(model.T, model.E, model.J, rule=jobc_E)
    model.jobc_BD = Constraint(model.T, model.BD, model.J, rule=jobc_BD)
    model.jobc_BE = Constraint(model.T, model.BE, model.J, rule=jobc_BE)
    
    #%% Model objectives 
    
    if optimization_type == "TWU":
        model.obj = Objective(rule=total_water_use, sense=minimize)
    elif optimization_type == "GWP":
        model.obj = Objective(rule=total_co2_emissions, sense=minimize)
    elif optimization_type == "NPC":
        model.obj = Objective(rule=net_present_cost, sense=minimize)
    else:
        raise ValueError("Undefined optimization objective: enter 'TWU', 'GWP', or 'NPC'.")
    
    #%% Instance creation
    
    instance = model.create_instance("input/param.dat")
    
    #%% Solution
        
    solver = SolverFactory('gurobi')
    result = solver.solve(instance, keepfiles=True)
    instance.solutions.load_from(result) 
    
    #%% Getting component values
    
    path = "./results/"+scenario+"/"
    
    postprocessing(instance, path, scenario)
        
    return value(instance.obj)
