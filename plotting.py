#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 14 10:35:41 2022

@author: Martino Piani, martino.piani@rwth-aachen.de
"""

import pandas

water = pandas.read_excel("results/graphs.xlsx", sheet_name="water", index_col=0)
co2 = pandas.read_excel("results/graphs.xlsx", sheet_name="co2", index_col=0)
jobs= pandas.read_excel("results/graphs.xlsx", sheet_name="jobs", index_col=0)

color_dict={
        'CCGT':	        'tab:gray',
        'Biomass':	    'tab:green',
        'Hydropower':   'tab:blue',
        'Solar_PV':     'tab:orange',
        'Wind':         'tab:red',
        'Geothermal':   'tab:brown',
        'BD_Castor':    'tab:purple',
        'BD_Jathropha': 'tab:olive',
        'ET_Sugarcane': 'tab:cyan',
        'electricity':  'y'
}

water.plot(color=color_dict, kind='barh', legend=True, stacked = True, xlabel="Scenario", title = "Total water use by source for each scenario", ylabel = "Water use [m3]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig("results/graphs/water.pdf", dpi=150)

co2.plot(color=color_dict, kind='barh', legend=True, stacked = True, xlabel="Scenario", title = "Total CO2 emissions by source for each scenario", ylabel = "Emissions [kgCO2eq]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig("results/graphs/co2.pdf", dpi=150)

jobs.plot(color=color_dict, kind='barh', legend=True, stacked = True, xlabel="Scenario", title = "Total job years created by source for each scenario", ylabel = "Job years", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig("results/graphs/jobs.pdf", dpi=150)
