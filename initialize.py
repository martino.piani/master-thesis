#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 15:15:18 2022

@author: Martino Piani, martino.piani@rwth-aachen.de
"""

#%% Initialization functions

def init_T(model):
    
    """Initiates the set of periods model.T (the model running years) as the 
    index of the corresponding data frame. """
    
    return model.data.index


def init_ED(model,t): 
    
    """Initiates model.ED with a time series of total electricity demand 
    from the corresponding data frame. """
    
    return model.data["electricity"][t]

def init_DED(model,t): 
    
    """Initiates model.DED with a time series of domestic electricity demand 
    from the corresponding data frame. """
    
    return model.data["electricity_dom"][t]

def init_BDD(model,t): 
    
    """Initiates model.BDD calculating it as a dynamic share of the diesel demand . """
    
    return model.data["biodiesel"][t]

def init_BED(model,t): 

    """Initiates model.BED calculating it as a dynamic share of the gasoline demand. """
    
    return model.data["bioethanol"][t]


def init_jobmult(model, t):
    return model.data["job_multiplier"][t]
