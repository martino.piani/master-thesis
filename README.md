# README

## Quick start

Before running the model fill the files _./input/param.dat_ and _./input/$SCENARIO/time_series.xlsx_.
Open the _run.py_, there is a dictionary that associates every scenario to its optimization objective.
Run the file, making sure you have Python installed, with the environment described in the file _env.yaml_.
The results will appear as graphs and as an _*.xlsx_ file in the _results_ folder, categorized by scenario. 

## The model

The core file of the optimization model is _WEF\_Opt.py_. The Pyomo model is constructed in that file and refers to the other auxiliary files.
The file _initialize.py_ contains information for initializing sets and parameters,
the file _constraints.py_ contains the constraint definitions of the model,
the file _objectives.py_ contains the definitions of the three optimization objectives,
and _postprocessing.py_ contains the generationof the output structure, as well as the code for generating graphs.
