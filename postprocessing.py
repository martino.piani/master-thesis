#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 29 09:48:22 2022

@author: Martino Piani, martino.piani@rwth-aachen.de

Postprocessing function for instance of pyomo modeling of the WEF Nexus in Ethiopia.

"""

import pandas

def postprocessing(instance, path = "", scenario = ""):
    
    # Sets values
    Periods = instance.T.ordered_data()
    E_sources=instance.E.ordered_data()
    BE_sources=instance.BE.ordered_data()
    BD_sources=instance.BD.ordered_data()
    
    # Capacity values
    E_capacity = pandas.DataFrame([[instance.c_e.extract_values()[(t,e)] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    E_production = pandas.DataFrame([[instance.L.extract_values()[(t,e)] for e in E_sources]
                                     for t in Periods], index=Periods, columns=E_sources)
    BD_capacity = pandas.DataFrame([[instance.c_bd.extract_values()[(t,b)] for b in BD_sources]
                                    for t in Periods], index=Periods, columns=BD_sources)
    BE_capacity = pandas.DataFrame([[instance.c_be.extract_values()[(t,b)] for b in BE_sources]
                                    for t in Periods], index=Periods, columns=BE_sources)   
    
    production = pandas.concat([E_production,BD_capacity,BE_capacity], axis=1)
    
    B_capacity = pandas.concat([BD_capacity,BE_capacity], axis=1)
    
    # Demand values
    Dom_demand = pandas.DataFrame().from_dict(instance.DED.extract_values(), orient='index',
                                            columns=["electricity"])
    E_demand = pandas.DataFrame().from_dict(instance.ED.extract_values(), orient='index',
                                            columns=["electricity"])
    BD_demand = pandas.DataFrame().from_dict(instance.BDD.extract_values(), orient='index',
                                             columns=["biodiesel"])
    BE_demand = pandas.DataFrame().from_dict(instance.BED.extract_values(), orient='index',
                                             columns=["bioethanol"])
    
    demand = pandas.concat([E_demand,BD_demand,BE_demand], axis=1)
    
    # Power surplus for export
    # print(Dom_demand)
    
    Tot_E_production = E_production.sum(axis=1).to_frame()
    Tot_E_production.columns = ["electricity"]
    res = Tot_E_production - Dom_demand # residues for export
    
    # Water use
    E_water_use = pandas.DataFrame([[instance.water_e.extract_values()[(t,e)] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    BD_water_use = pandas.DataFrame([[instance.water_bd.extract_values()[(t,b)] for b in BD_sources]
                                    for t in Periods], index=Periods, columns=BD_sources)
    BE_water_use = pandas.DataFrame([[instance.water_be.extract_values()[(t,b)] for b in BE_sources]
                                    for t in Periods], index=Periods, columns=BE_sources)
    
    water_use = pandas.concat([E_water_use,BD_water_use,BE_water_use], axis=1)
    
    # CO2 emissions
    E_carbon = pandas.DataFrame([[instance.co2_e.extract_values()[(t,e)] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    BD_carbon = pandas.DataFrame([[instance.co2_bd.extract_values()[(t,b)] for b in BD_sources]
                                    for t in Periods], index=Periods, columns=BD_sources)
    BE_carbon = pandas.DataFrame([[instance.co2_be.extract_values()[(t,b)] for b in BE_sources]
                                    for t in Periods], index=Periods, columns=BE_sources)
    
    carbon_emissions = pandas.concat([E_carbon,BD_carbon,BE_carbon], axis=1)
    
    # Investment
    E_inv = pandas.DataFrame([[instance.inv_cost_e.extract_values()[(t,e)] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    BD_inv = pandas.DataFrame([[instance.inv_cost_bd.extract_values()[(t,b)] for b in BD_sources]
                                    for t in Periods], index=Periods, columns=BD_sources)
    BE_inv = pandas.DataFrame([[instance.inv_cost_be.extract_values()[(t,b)] for b in BE_sources]
                                    for t in Periods], index=Periods, columns=BE_sources)
    
    investment = pandas.concat([E_inv,BD_inv,BE_inv], axis=1)
    
    # Operation
    E_op = pandas.DataFrame([[instance.op_cost_e.extract_values()[(t,e)] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    BD_op = pandas.DataFrame([[instance.op_cost_bd.extract_values()[(t,b)] for b in BD_sources]
                                    for t in Periods], index=Periods, columns=BD_sources)
    BE_op = pandas.DataFrame([[instance.op_cost_be.extract_values()[(t,b)] for b in BE_sources]
                                    for t in Periods], index=Periods, columns=BE_sources)
    
    operation = pandas.concat([E_op,BD_op,BE_op], axis=1)
    
    # Jobs 
    mfc = pandas.DataFrame([[instance.jobs_e.extract_values()[(t,e,"MFC")] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    ci = pandas.DataFrame([[instance.jobs_e.extract_values()[(t,e,"CI")] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    om = pandas.DataFrame([[instance.jobs_e.extract_values()[(t,e,"OM")] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    FJ_E = pandas.DataFrame([[instance.jobs_e.extract_values()[(t,e,"FJ")] for e in E_sources]
                                   for t in Periods], index=Periods, columns=E_sources)
    
    FJ_BD = pandas.DataFrame([[instance.jobs_bd.extract_values()[(t,bd,"FJ")] for bd in BD_sources]
                                   for t in Periods], index=Periods, columns=BD_sources) 
    FJ_BE = pandas.DataFrame([[instance.jobs_be.extract_values()[(t,be,"FJ")] for be in BE_sources]
                                   for t in Periods], index=Periods, columns=BE_sources) 
   
    fj = pandas.concat([FJ_E, FJ_BD, FJ_BE], axis=1)
    
    #%% Electricity mix
    
    Tot_E_Capacity = E_capacity.sum(axis=1)
    mix = E_capacity.divide(Tot_E_Capacity.to_numpy(), axis="index")
    
    #%% Evaluation
    
    total_energy = Tot_E_production.sum(axis=1).sum(axis=0) * 3.6 + B_capacity.sum(axis=1).sum(axis=0)
    total_water = water_use.sum(axis=0).sum(axis=0)
    total_cost = investment.sum(axis=1).sum(axis=0) + operation.sum(axis=1).sum(axis=0)
    total_emissions = carbon_emissions.sum(axis=1).sum(axis=0)
    total_jobs = ci.sum(axis=1).sum(axis=0) + om.sum(axis=1).sum(axis=0) + fj.sum(axis=1).sum(axis=0)
    
    WI = total_water/total_energy
    CI = total_emissions/total_energy
    JI = total_jobs/total_cost
    COE = total_cost/total_energy
    
    evaluation = pandas.DataFrame([
        ["total_energy",total_energy,"GJ"],
        ["total_water",total_water,"m3"],
        ["total_cost",total_cost,"USD"],
        ["total_emissions",total_emissions,"kgCO2eq"],
        ["total_jobs",total_jobs,"job years"],
        ["WI",WI,"m3/GJ"],
        ["CI",CI,"kgCO2eq/GJ"],
        ["JI",JI*1000,"job years/1000USD"],
        ["COE",COE,"USD/GJ"]])
    
    #%% Save raw data to excel in different sheets
    
    with pandas.ExcelWriter(path+"results_"+scenario+".xlsx",engine='openpyxl') as writer:
        E_capacity.to_excel(writer, sheet_name="capacity")
        mix.to_excel(writer, sheet_name="mix")
        production.to_excel(writer, sheet_name="production")
        demand.to_excel(writer, sheet_name="demand")
        water_use.to_excel(writer, sheet_name="water_use")
        carbon_emissions.to_excel(writer, sheet_name="carbon_emissions")
        investment.to_excel(writer, sheet_name="investment")
        operation.to_excel(writer, sheet_name="operation")
        mfc.to_excel(writer, sheet_name="MFC")
        ci.to_excel(writer, sheet_name="C&I")
        om.to_excel(writer, sheet_name="O&M")
        fj.to_excel(writer, sheet_name="fuel jobs")
        evaluation.to_excel(writer, sheet_name="evaluation", index=False)
        writer.save()
    
    #%% Plotting
    
    color_dict={
        'CCGT':	        'tab:gray',
        'Biomass':	    'tab:green',
        'Hydropower':   'tab:blue',
        'Solar_PV':     'tab:orange',
        'Wind':         'tab:red',
        'Geothermal':   'tab:brown',
        'BD_Castor':    'tab:purple',
        'BD_Jathropha': 'tab:olive',
        'ET_Sugarcane': 'tab:cyan',
        'electricity':  'y'
}
   
    E_capacity.plot(color=color_dict, kind='bar', legend=True, stacked = True, xlabel="Year", title = "Installed Power Capacity: "+scenario+" scenario", ylabel = "Capacity [MW]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig(path+"capacity.pdf", dpi=150)

    mix.plot(color=color_dict, kind='bar', legend=True, stacked = True, xlabel="Year", title = "Electricity mix: "+scenario+" scenario", ylabel = "Share [-]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig(path+"mix.pdf", dpi=150)

    B_capacity.plot(color=color_dict, kind='bar', legend=True, stacked = True, xlabel="Year", title = "Biofuel Production: "+scenario+" scenario", ylabel = "Capacity [GJ/a]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig(path+"B_capacity.pdf", dpi=150)

    water_use.plot(color=color_dict, kind='bar', legend=True, stacked = True, xlabel="Year", title = "Water Use: "+scenario+" scenario", ylabel = "Water use [m3]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig(path+"water.pdf", dpi=150)
    
    carbon_emissions.plot(color=color_dict, kind='bar', legend=True, stacked = True, xlabel="Year", title = "Carbon Emissions: "+scenario+" scenario", ylabel = "Emissions [kg CO2 eq]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig(path+"emissions.pdf", dpi=150)
    
    investment.plot(color=color_dict, kind='bar', legend=True, stacked = True, xlabel="Year", title = "Investment costs: "+scenario+" scenario", ylabel = "Cost [USD]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig(path+"investment.pdf", dpi=150)
    
    operation.plot(color=color_dict, kind='bar', legend=True, stacked = True, xlabel="Year", title = "Operation costs: "+scenario+" scenario", ylabel = "Cost [USD]", figsize = (9,6), width = 0.9, sort_columns = True).get_figure().savefig(path+"operation.pdf", dpi=150)
    
    res.plot(color=color_dict, kind="bar",title = "Electricity surplus for export: "+scenario+" scenario", xlabel="Year", ylabel="Production [MWh]", figsize = (9,6), width = 0.9).get_figure().savefig(path+"power_surplus.pdf", dpi=150)
    
    mfc.plot(color=color_dict, kind="bar",title = "Manufacturing jobs: "+scenario+" scenario", xlabel="Year", ylabel="Job years", stacked = True, figsize = (9,6), width = 0.9).get_figure().savefig(path+"mfc_jobs.pdf", dpi=150)
    
    ci.plot(color=color_dict, kind="bar",title = "Construction & installation jobs: "+scenario+" scenario", xlabel="Year", ylabel="Job years", stacked = True, figsize = (9,6), width = 0.9).get_figure().savefig(path+"c&i_jobs.pdf", dpi=150)

    om.plot(color=color_dict, kind="bar",title = "O&M jobs: "+scenario+" scenario", xlabel="Year", ylabel="Job years", stacked = True, figsize = (9,6), width = 0.9).get_figure().savefig(path+"o&m_jobs.pdf", dpi=150)

    fj.plot(color=color_dict, kind="bar",title = "Fuel jobs: "+scenario+" scenario", xlabel="Year", ylabel="Job years", stacked = True, figsize = (9,6), width = 0.9).get_figure().savefig(path+"fuel_jobs.pdf", dpi=150)
    
    